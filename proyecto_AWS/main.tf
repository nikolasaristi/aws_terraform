module "resource_group" {
    source = "./modules/resource-group"
    resource_group_name = var.resource_group_name
    
}

module "virtual_machine" {
    source = "./modules/virtual-machine"
    resource_group_name = var.resource_group_name

}

module "fargate" {
    source = "./modules/fargate"
    
}